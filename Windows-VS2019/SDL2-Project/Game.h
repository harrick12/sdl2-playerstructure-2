#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include "Player.h"

typedef struct Game {
	static const int WINDOW_WIDTH = 800;
	static const int WINDOW_HEIGHT = 600;

	// Declare window and renderer objects
	SDL_Window* gameWindow = nullptr;
	SDL_Renderer* gameRenderer = nullptr;

	// Texture which stores the actual sprite (this
	// will be optimised).
	SDL_Texture* backgroundTexture = nullptr;

	//Declare Player
	Player player;

	// Window control
	SDL_Event event;
	bool quit = false; //false

	// Keyboard
	const Uint8* keyStates;

	// Timing variables
	unsigned int currentTimeIndex;
	unsigned int prevTimeIndex;
	unsigned int timeDelta;
	float timeDeltaInSeconds;

} Game;

bool initGame(Game* game);
void runGameLoop(Game* game);
void processInputs(Game* game);
void updateGame(Game* game);
void drawGame(Game* game);
void cleanUpGame(Game* game);
#endif